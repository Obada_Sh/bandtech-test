# bandtech-test



## Overview

This project is designed as part of the application process for a Fullstack Developer position at BandTech. It implements a complete backend solution using the Laravel framework, addressing the provided task requirements. The system handles user and product management with distinct user types influencing dynamic product pricing.

## Features


- User Authentication: Login and registration systems to handle user authentication securely.
- User Management: APIs to create, read, update, and delete user information.
- Product Management: APIs to manage product listings, including creation, retrieval, updating, and deletion.
- Dynamic Pricing: Different pricing for products based on user types (normal, gold, and silver).

## Technologies Used


- Laravel: For the backend framework.
- MySQL: As the database for users and products.
- JWT : I used JWT to generate tokens for authentication.
- Postman: For API testing and documentation.

## Installation

Follow these steps to set up the project environment and run the application:
1- Clone the repository:
    git clone [https://gitlab.com/Obada_Sh/bandtech-test.git]
    cd [bandtech-test]
2 - Install dependencies:
    composer install
3 - Set up the environment file:
    Copy the '.env.example' file to a new file named .env and adjust the database and other configurations as necessary.
    cp .env.example .env
4 - Generate an application key:
    php artisan key:generate
5 - Run migrations:
    This will set up your database with the required tables.
    php artisan migrate
6 - Generate JWT key:
    This will generate a unique key used to sign and verify JSON Web Tokens (JWTs). 
    php artisan jwt:secret
7 - Create storage link : 
    php artisan storage:link.


## API Usage

Access the APIs through the uploaded postman collection named api_collections.json in the project files




