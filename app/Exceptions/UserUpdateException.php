<?php

namespace App\Exceptions;

use Exception;

class UserUpdateException extends Exception
{
    protected $message;
    protected $code;

    public function __construct($message = "Failed to update user", $code = 400)
    {
        parent::__construct($message, $code);
    }
}
