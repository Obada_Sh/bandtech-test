<?php

namespace App\Exceptions;

use Exception;

class UserCreationException extends Exception
{
    protected $message;
    protected $code;

    public function __construct($message = "Failed to create user", $code = 400)
    {
        parent::__construct($message, $code);
    }
}
