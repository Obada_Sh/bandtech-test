<?php

namespace App\Exceptions;

use Exception;

class ProductCreatinoException extends Exception
{
    protected $message;
    protected $code;

    public function __construct($message = "Failed to create product", $code = 400)
    {
        parent::__construct($message, $code);
    }
}
