<?php

namespace App\Exceptions;

use Exception;

class ProductUpdateException extends Exception
{
    protected $message;
    protected $code;

    public function __construct($message = "Failed to update product", $code = 400)
    {
        parent::__construct($message, $code);
    }
}
