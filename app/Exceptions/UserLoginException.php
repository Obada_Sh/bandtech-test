<?php

namespace App\Exceptions;

use Exception;

class UserLoginException extends Exception
{
    protected $message;
    protected $code;

    public function __construct($message = "Login Failed", $code = 400)
    {
        parent::__construct($message, $code);
    }
}
