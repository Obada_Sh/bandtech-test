<?php
namespace App\Repositories;

interface AuthRepositoryInterface
{
    public function create(array $data);
    public function findByUsername(array $username);
}
