<?php

namespace App\Repositories;

use Exception;
use App\Models\User;
use App\Models\Product;
use App\Traits\StorePhoto;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use App\Exceptions\UserNotFoundException;
use App\Exceptions\ProductUpdateException;
use App\Exceptions\ProductCreatinoException;
use App\Exceptions\ProductNotFoundException;
use App\Repositories\ProductRepositoryInterface;

class ProductRepository implements ProductRepositoryInterface
{
    use ResponseTrait;
    use StorePhoto;
    protected Product $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    // function to return all the products
    public function getAll()
    {
        return $this->product
            ->get();
    }

    // function to return specific product according to id
    public function getById(int $id)
    {
        $product =  $this->product->where('id', $id)->get();

        if (!$product) {
            throw new ProductNotFoundException();
        }
        return $product;

    }

    // function to create the product record
    public function create(array $data)
    {
        try{
            DB::beginTransaction();
            $product = new $this->product;
            $product->name = $data['name'];
            $product->description = $data['description'];
            if (isset($data['image'])) {
                $product->image = $this->store($data['image'], 'ProductImages');
            }else{
                $product->image =null;
            }
            $product->price = $data['price'];
            $product->slug = $data['slug'];
            $product->is_active = $data['is_active'] ?? true;
            $product->save();

            DB::commit();
            return $product->fresh();
            }catch(Exception $e){
                    DB::rollBack();
                    throw new ProductCreatinoException(("Unable to create product: "). $e->getMessage());

                }
    }

    // function to update product record
    public function update(array $data, int $id)
    {

        try{
            DB::beginTransaction();
            $product = $this->product->find($id);

            if (!$product) {
                throw new ProductNotFoundException();
            }

            $product->name = $data['name'] ?? $product->name;
            $product->description = $data['description'] ?? $product->description;
            if (isset($data['image'])) {
                $product->image = $this->store($data['image'], 'ProductImages');
            }
            $product->price = $data['price'] ?? $product->price;
            $product->slug = $data['slug'] ?? $product->slug;
            $product->is_active = $data['is_active'] ?? $product->is_active;

        $product->update();

        DB::commit();
            return $product->fresh();
            }catch(Exception $e){
                    DB::rollBack();
                    throw new ProductUpdateException(("Unable to update product: "). $e->getMessage());

                }
    }

    // function to delete product record
    public function delete($id)
    {

        $product = $this->product->find($id);

        if (!$product) {
            throw new ProductNotFoundException();
        }

        $product->delete();

        return $product;
    }

    public function findPricesByUserId(int $productId, int $userId)
    {
        $product = Product::find($productId);

        if (!$product) {
            throw new ProductNotFoundException();
        }
        $user = User::find($userId);

        if (!$user) {
            throw new UserNotFoundException();
        }

        switch ($user->type) {
            case 'gold':
                return $product->price * 0.9; // 10% discount for gold users
            case 'silver':
                return $product->price * 0.95; // 5% discount for gold users
            default:
                return $product->price; // No discount for normal users
        }
    }

}
