<?php

namespace App\Repositories;

use Exception;
use App\Models\User;
use App\Traits\StorePhoto;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\UserLoginException;
use App\Exceptions\UserCreationException;
use App\Repositories\AuthRepositoryInterface;

class AuthRepository implements AuthRepositoryInterface
{
    use ResponseTrait;
    use StorePhoto;

    protected User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
    // function to create the user record
    public function create(array $data)
    {

        try{

            DB::beginTransaction();

            $user = $this->user->create([
                'name' => $data['name'],
                'username' => $data['username'],
                'password' => $data['password'],
                'type' => $data['type'] ?? 'normal',
                'is_active' => $data['is_active'] ?? true,
            ]);

            if (isset($data['avatar'])) {
                $user->avatar = $this->store($data['avatar'], 'UsersAvatar');
            } else {
                $user->avatar = null;
            }

            $user->save();

            $token = JWTAuth::fromUser($user);

            DB::commit();
            return $user = self::userWithToken($user,$token);
        }catch(Exception $e){
                DB::rollBack();
                throw new UserCreationException(("Unable to create user: "). $e->getMessage());

            }

    }

    // functoin to return the user according to username
    public function findByUsername(array $credentials)
    {
        try{

            DB::beginTransaction();

            if (Auth::attempt(['username' => $credentials['username'],'password' => $credentials['password'],]))
            {
                $user = User::where('username', $credentials['username'])->first();

                // here the data type is string and it's defined in JWTAuth package method declaration
                $token = JWTAuth::fromUser($user);

                DB::commit();
                return $user = self::userWithToken($user,$token);
        }else
        {
            throw new UserLoginException("Bad Credentials");
        }
    }catch(Exception $e){
                DB::rollBack();
                throw new UserLoginException(__("Unable to login: "). $e->getMessage());
            }
    }
}
