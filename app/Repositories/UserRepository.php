<?php

namespace App\Repositories;

use Exception;
use App\Models\User;
use App\Traits\StorePhoto;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use App\Exceptions\UserUpdateException;
use App\Exceptions\UserCreationException;
use App\Exceptions\UserNotFoundException;
use App\Repositories\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
    use ResponseTrait;
    use StorePhoto;
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    // function to return all the users
    public function getAll()
    {
        return $this->user
            ->get();
    }

    // function to return specific user according to id

    public function getById(int $id)
    {
        $user = $this->user->where('id', $id)->first();

        if (!$user) {
            throw new UserNotFoundException();
        }

        return $user;
    }

    // function to create the user record
    public function create(array $data)
    {
        try{
            DB::beginTransaction();
            $user = new $this->user;
            $user->name = $data['name'];
            $user->username = $data['username'];
            $user->password = $data['password'];
            $user->type = $data['type'] ?? 'normal';
            $user->is_active = $data['is_active'] ?? true;
            if (isset($data['avatar'])) {
                $user->avatar = $this->store($data['avatar'], 'UsersAvatar');
            } else {
                $user->avatar = null;
            }

            $user->save();

            DB::commit();
            return $user->fresh();
            }catch(Exception $e){
                    DB::rollBack();
                    throw new UserCreationException(("Unable to create user: "). $e->getMessage());

                }
    }

    // function to update user record
    public function update(array $data, int $id)
    {

        try{
            DB::beginTransaction();
            $user = $this->user->find($id);

            if (!$user) {
                throw new UserNotFoundException();
            }

        $user->name = $data['name'] ?? $user->name;
        $user->username = $data['username'] ?? $user->username;
        $user->password = $data['password'] ?? $user->password;
        $user->type = $data['type'] ?? $user->type;
        $user->is_active = $data['is_active'] ?? $user->is_active;
        if (isset($data['avatar'])) {
            $user->avatar = $this->store($data['avatar'], 'UsersAvatar');
        }

        $user->update();

        DB::commit();
            return $user->fresh();
            }catch(Exception $e){
                    DB::rollBack();
                    throw new UserUpdateException(("Unable to update user: "). $e->getMessage());

                }
    }

    // function to delete user record
    public function delete($id)
    {

        $user = $this->user->find($id);
        if (!$user) {
            throw new UserNotFoundException();
        }
        $user->delete();

        return $user;
    }

}
