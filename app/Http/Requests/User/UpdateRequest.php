<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'string|max:255',
            'username' => 'string|max:255|unique:users',
            'password' => 'string|min:8',
            'type' => 'in:gold,silver,normal',
            'is_active' => 'sometimes|boolean',
            'avatar' => 'nullable|image|max:2048', // 2MB max file size, only image files
        ];
    }
}
