<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'string|max:255', // max length based on typical DB VARCHAR
            'description' => 'string',
            'image' => 'nullable|image|max:2048', // 2MB max file size, only image files
            'price' => 'numeric|min:0|max:999999.99', // Adjust range as needed
            'slug' => 'string|max:255|unique:products,slug', // Ensure slug is unique in the products table
            'is_active' => 'sometimes|boolean', // 'sometimes' if this field can be omitted in some cases
        ];
    }
}
