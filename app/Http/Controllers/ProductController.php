<?php

namespace App\Http\Controllers;


use App\Services\ProductService;
use App\Http\Requests\Product\ProductCreateRequest;
use App\Http\Requests\Product\ProductUpdateRequest;

class ProductController extends Controller
{
    protected ProductService $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function create(ProductCreateRequest $request)
    {
        return $this->productService->create($request);
    }

    public function update(ProductUpdateRequest $request, $id)
    {
        return $this->productService->update($request, $id);
    }

    public function delete(int $id)
    {
        return $this->productService->delete($id);
    }

    public function getById(int $id)
    {
        return $this->productService->getById($id);
    }

    public function getAll()
    {
        return $this->productService->getAll();
    }
    public function findPricesByUserId(int $productId, int $userId)
    {
        return $this->productService->findPricesByUserId($productId, $userId);
    }
    
}
