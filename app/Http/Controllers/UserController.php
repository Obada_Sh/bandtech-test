<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use App\Http\Requests\User\CreateRequest;
use App\Http\Requests\User\UpdateRequest;

class UserController extends Controller
{
    protected UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function create(CreateRequest $request)
    {
        return $this->userService->create($request);
    }

    public function update(UpdateRequest $request, $id)
    {
        return $this->userService->update($request, $id);
    }

    public function delete($id)
    {
        return $this->userService->delete($id);
    }

    public function getById($id)
    {
        return $this->userService->getById($id);
    }

    public function getAll()
    {
        return $this->userService->getAll();
    }
}
