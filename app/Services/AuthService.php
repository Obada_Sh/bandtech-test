<?php


namespace App\Services;

use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Hash;
use App\Exceptions\UserLoginException;
use App\Exceptions\UserCreationException;
use App\Repositories\AuthRepositoryInterface;
use Illuminate\Http\Request;

class AuthService
{
    use ResponseTrait;
    protected AuthRepositoryInterface $authRepository;

    public function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepository = $authRepository;
    }

    public function register(Request $data)
    {
        try {

            $data['password'] = Hash::make($data['password']);
            $user = $this->authRepository->create($data->only('name', 'username', 'password','avatar','type','is_active'));

            return $this->successWithData($user, 201, 'Registered successfully');
        }catch (UserCreationException $e) {
            return $this->failed($e->getMessage(), 422);
        }


    }

    public function login(Request $credentials)
    {
        try {

            $user = $this->authRepository->findByUsername($credentials->only('username', 'password'));

            return $this->successWithData($user, 200, 'Loggedin successfully');
        }catch (UserLoginException $e) {
            return $this->failed($e->getMessage(), 422);
        }
    }
}
