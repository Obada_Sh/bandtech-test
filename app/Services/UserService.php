<?php


namespace App\Services;

use Illuminate\Http\Request;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Hash;
use App\Exceptions\UserUpdateException;
use App\Exceptions\UserCreationException;
use App\Exceptions\UserNotFoundException;
use App\Repositories\UserRepositoryInterface;

class UserService
{
    use ResponseTrait;
    protected UserRepositoryInterface $userRepository;
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }


    public function create(Request $data)
    {
        try {

            $data['password'] = Hash::make($data['password']);
            $user = $this->userRepository->create($data->only('name', 'username', 'password','avatar','type','is_active'));

            return $this->successWithData($user, 201, 'created successfully');
        }catch (UserCreationException $e) {
            return $this->failed($e->getMessage(), 422);
        }
    }


    public function getAll()
    {

        $data = $this->userRepository->getAll();

        return $this->successWithData($data, 200, 'operation completed');
    }


    public function getById(int $id)
    {
        try {
            $data = $this->userRepository->getById($id);
            return $this->successWithData($data, 200, 'Operation completed');
        } catch (UserNotFoundException $e) {
            return $this->failed($e->getMessage(), 404);
        }
    }


    public function update(Request $data, int $id)
    {
        try {

            if ($data->has('password')) {
                $data['password'] = Hash::make($data['password']);
            }
            $user = $this->userRepository->update($data->only('name', 'username', 'password','avatar','type','is_active'), $id);

            return $this->successWithData($user, 200, 'updated successfully');
        }catch (UserNotFoundException $e) {
            // Return a 404 response if no user is found with the given ID
            return $this->failed($e->getMessage(), 404);
        }catch (UserUpdateException $e) {
            return $this->failed($e->getMessage(), 422);
        }
    }


    public function delete(int $id)
    {
        try {
            $this->userRepository->delete($id);
            return $this->successWithData('', 200, 'User deleted successfully');
        } catch (UserNotFoundException $e) {
            return $this->failed($e->getMessage(), 404);
        }
    }

}
