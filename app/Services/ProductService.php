<?php


namespace App\Services;

use Illuminate\Http\Request;
use App\Traits\ResponseTrait;
use App\Exceptions\UserNotFoundException;
use App\Exceptions\ProductUpdateException;
use App\Exceptions\ProductCreatinoException;
use App\Exceptions\ProductNotFoundException;
use App\Repositories\ProductRepositoryInterface;

class ProductService
{
    use ResponseTrait;
    protected ProductRepositoryInterface $productRepository;
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function create(Request $data)
    {
        try {
            $product = $this->productRepository->create($data->only('name', 'description', 'image','price','type','slug','is_active'));

            return $this->successWithData($product, 201, 'created successfully');
        }catch (ProductCreatinoException $e) {
            return $this->failed($e->getMessage(), 422);
        }
    }

    public function getAll()
    {

        $data = $this->productRepository->getAll();

        return $this->successWithData($data, 200, 'operation completed');
    }

    public function getById(int $id)
    {
        try {
            $data = $this->productRepository->getById($id);
            return $this->successWithData($data, 200, 'Operation completed');
        } catch (ProductNotFoundException $e) {
            return $this->failed($e->getMessage(), 404);
        }
    }

    public function update(Request $data, int $id)
    {
        try {

            $product = $this->productRepository->update($data->only('name', 'description', 'image','price','type','slug','is_active'), $id);

            return $this->successWithData($product, 200, 'updated successfully');
        }catch (ProductNotFoundException $e) {
            // Return a 404 response if no product is found with the given ID
            return $this->failed($e->getMessage(), 404);
        }catch (ProductUpdateException $e) {
            return $this->failed($e->getMessage(), 422);
        }
    }

    public function delete(int $id)
    {
        try {
            $this->productRepository->delete($id);
            return $this->successWithData('',200,'product deleted successfully');
        } catch (ProductNotFoundException $e) {
            return $this->failed($e->getMessage(), 404);
        }
    }

    public function findPricesByUserId(int $productId, int $userId)
    {
        try {
            $price = $this->productRepository->findPricesByUserId($productId, $userId);
            return $this->successWithData($price,200,'operation completed successfully');
        }catch (UserNotFoundException $e) {
            return $this->failed($e->getMessage(), 404);
        }catch (ProductNotFoundException $e) {
            return $this->failed($e->getMessage(), 404);
        }

    }

}
