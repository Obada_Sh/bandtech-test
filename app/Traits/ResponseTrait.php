<?php

namespace App\Traits;

use Tymon\JWTAuth\Facades\JWTAuth;

trait ResponseTrait{

    static function successWithMessage($messageHolder,$statusCode = 200){

        return response()->json([
            'status' => true,
            'message' => __($messageHolder)
        ],$statusCode);

    }

    static function successWithData($data,$statusCode = 200,$messageHolder){

        return response()->json([
            'status' => true,
            'data' =>  $data,
            'message' => __($messageHolder)
        ],$statusCode);

    }

    static function userWithToken($user,$token)
    {
        $user->access_token = $token;
        $user->token_type =  'bearer';
        $user->expires_in = JWTAuth::factory()->getTTL() ;
        return $user;
    }


    static function failed($messageHolder,$statusCode = 400){

        return response()->json([
            'status' => false,
            'message' => __($messageHolder)
        ],$statusCode);

    }


}
