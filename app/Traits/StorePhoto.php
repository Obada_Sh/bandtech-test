<?php

namespace App\Traits;

use Illuminate\Support\Facades\URL;

trait StorePhoto{

    static function store($photo,$location){

        $fileNameToStore = time() . '_' . uniqid() . '.' . $photo->extension();

        $photo->storeAs($location, $fileNameToStore, 'public');

        $path = URL::asset("storage/".$location."/".$fileNameToStore);

        return $path;

    }

}
