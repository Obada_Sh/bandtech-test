<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

Route::prefix('/users')->group(function(){
    Route::get('', [UserController::class,'getAll']);
    Route::get('/{id}', [UserController::class,'getById']);
    Route::delete('delete/{id}', [UserController::class,'delete']);
    Route::put('update/{id}', [UserController::class,'update']);
    Route::post('create', [UserController::class,'create']);
});

Route::prefix('/products')->group(function(){
    Route::get('', [ProductController::class,'getAll']);
    Route::get('/{id}', [ProductController::class,'getById']);
    Route::get('/price/{productId}/{userId}', [ProductController::class,'findPricesByUserId']);
    Route::delete('delete/{id}', [ProductController::class,'delete']);
    Route::put('update/{id}', [ProductController::class,'update']);
    Route::post('create', [ProductController::class,'create']);
});

